from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from jinja2 import Template

class StaticTestServer:
	def __init__(self, theme, descriptor):
		self.theme = json.load(open("templates/"+theme+"/theme.json", "rb"))
		self.descriptor = json.load(open(descriptor+".json", "rb"))
		
	def render_page(self, page):
		page = self.descriptor.get("pages").get(page)

		start_render(page)

		out = ""
		out+="<!DOCTYPE html>\n"
		out+="<html>\n"

		out+="<head>\n"
		out += self.header
		out+="</head>\n"

		render(self.content)

		out+="</html>\n"

		return out

	def start_render(page):
		requirements = flatten_requirements(page)

		self.header = render_head(requirements)

		base_template = page.get("base")
		base_template = self.theme.get("templates").get(base_template)

		self.content = base_template


	def flatten_requirements(page):
		reqs = self.theme.get("require")
		base_template = page.get("base")
		base_template = self.theme.get("templates").get(base_template)

		return _flatten_requirements(reqs, base_template)

	def _flatten_requirements(ret, page_desc):
		js = ret["js"]
		script = ret["script"]
		css = ret["css"]
		ie = ret["ie-compat"]

		if page_desc.get("template"):
			t = self.theme.get("components").get(page_desc.get("template")).get("require", {"js":[], "script":[], "css":[], "ie-compat":[]})
			js.extend(t.get("js"))		
			script.extend(t.get("script"))		
			css.extend(t.get("css"))		
			ie.extend(t.get("ie-compat"))		
		elif page_desc.get("tag"):
			for c in page_desc.get("children"):
				_flatten_requirements(ret, c)

		return {"js":js, "script":script, "css":script, "ie-compat":ie}

	def render_head(requirements):
		ret = ""
		for css in requirements.get("css"):
			ret +="<link rel='stylesheet' href='%s' >\n" % css.get("url")
		for js in requirements.get("js"):
			ret +="<script src='%s' ></script>\n" % css.get("url")
		ret +="""
		<script>
		$(function(){"""
		for script in requirements.get("script"):
			ret +=script+"\n";
		ret +="""
		});
		</script>"""
		return ret


	def render_tag(page_desc):
		ret = "<"+page_desc.get("tag")
		for k in ret:
			if not k in ["tag", "children"]:
				ret +=" %s='%s'" % (k, " ".join(ret[k]))
		ret += " >"
		for c in children:
			ret +=render(c.get("type"))(c)
		ret +="</"+page_desc.get("tag")
		return ret

	def render_template(page_desc):
		ret = ""
		t = self.theme.get("components").get(page_desc.get("template"))
		tn = t.get("name")
		ps = t.get("params")
		#it's code
		if t.get("content"):
			for tc in t.get("content"):
				ret += render(tc.get("type"))(tc)
		#it's a file
		else:
			
		return ret

	
	def render_var(page_desc):
		return "{{"+page_desc.get("var")+"}}"


	def render(mode):

		if mode == "tag":
			return render_tag
		elif mode == "template":
			return render_template
		elif mode =="var":
			return render_var


    def dispatch_request(self, request):
        return Response('Hello World!')

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = Response(render_page(request.path[1:]))
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

		
		
		
if __name__ == '__main__':
	params = 
	run_simple('0.0.0.0', 4000, application)